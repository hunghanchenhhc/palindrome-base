package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		Boolean result = Palindrome.isPalindrome("abccba");
		assertTrue("Input is not match result",result);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		Boolean result = Palindrome.isPalindrome("word");
		assertFalse("Input is not match result",result);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		Boolean result = Palindrome.isPalindrome("edit tide");
		assertTrue("Input is not match result",result);
		}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		Boolean result = Palindrome.isPalindrome("edit on tide");
		assertFalse("Input is not match result",result);
	}	
	
}
